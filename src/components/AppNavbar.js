import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="black" variant="dark" expand="lg">
      <Navbar.Brand as={Link} to="/" className="mx-5">
        ClickShop
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link as={NavLink} to="/">
            Home
          </Nav.Link>
          {user.isAdmin ? (
            <Nav.Link as={NavLink} to="/dashboard">
              Dashboard
            </Nav.Link>
          ) : null}
          {user.isAdmin ? null : (
            <Nav.Link as={NavLink} to="/products">
              Products
            </Nav.Link>
          )}
          {user.id !== null ? (
            <Nav.Link as={NavLink} to="/logout">
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/login">
                Login
              </Nav.Link>
              <Nav.Link as={NavLink} to="/register">
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
