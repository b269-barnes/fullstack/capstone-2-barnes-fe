import { Button, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {
    const {title, content, label} = data;

    return (
        <Row className="p-5 text-center">
            <Link to="/products" style={{ textDecoration: 'none', color: 'inherit' }}>
                <h1>{title}</h1>
            </Link>
            <p>{content}</p>
        </Row>
    )
}
