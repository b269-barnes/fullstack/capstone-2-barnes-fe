import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Fashion</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Our fashion line is designed to help you make a statement, express your personality, and turn heads. We believe that fashion is not just about following trends but creating your own unique style. That's why our collection features a variety of styles and looks to cater to everyone's preferences. From bold prints and patterns to classic neutrals, our clothing and accessories are designed to help you stand out from the crowd. At ClickShop, we're passionate about fashion and want to help you express yourself through your wardrobe.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Health and Beauty</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Our health and beauty collection features high-quality skincare products, makeup, and hair care essentials that cater to all skin types and beauty needs. We believe that everyone deserves to feel beautiful and confident in their own skin, which is why our selection is carefully curated to cater to a diverse range of skin tones, textures, and concerns. Our products are designed to help you look and feel your best every day.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Sports and Outdoor</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Our Sports and Outdoor collection is designed to inspire adventure and encourage exploration. Whether you're a seasoned athlete or a weekend adventurer, our selection of gear and equipment is designed to help you push your limits and try new things. We believe that getting outside and being active is essential for a healthy and fulfilling lifestyle.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}