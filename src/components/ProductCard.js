
import { Link } from "react-router-dom";
import { Card, Button, Col } from "react-bootstrap";

export default function ProductCard({ productProp }) {
  const { _id, name, description, price, stock } = productProp;

  return (
    <Col xs={12} sm={6} md={4} className="mb-4">
      <Card className="cardHighlight p-3">
        <Card.Body>
          <h3 className = "text-center">{name}</h3>
         
          <p >{description}</p>
          <Card.Subtitle>Price: </Card.Subtitle>
         <Card.Text style={{ color: 'red' }}>${price}</Card.Text>
           <Card.Subtitle>Stocks: </Card.Subtitle>
          <Card.Text>{stock}</Card.Text>
          <Button as={Link} to={`/products/${_id}`} variant="primary">
            Details
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
}
