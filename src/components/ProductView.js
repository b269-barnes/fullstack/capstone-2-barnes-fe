import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import {  Card, Button } from "react-bootstrap";

import UserContext from "../UserContext";

export default function ProductView(){

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams();
  const { userId } = useParams()

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [quantity, setQuantity] = useState(1);
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then(res => res.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stock);
      })
      .catch(err => console.log(err));
  }, [productId]);

  const handleOrderCountChange = (e) => {
    setQuantity(e.target.value);
  };

  const handleBuyNow = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        userId: userId,
        productId: productId,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data && data.order) {
          Swal.fire({
            title: 'Successfully checked out!',
            icon: 'success',
            text: 'You have successfully checked out this product.',
          });
          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: data.message || 'Please try again.',
          });
        }
      })
      .catch((err) => console.log(err));
  };


  return (
    <Card>
      <Card.Header className = "text-center bg-black" style={{ textTransform: 'uppercase' , fontWeight: "" , color: "white"  }} >{name}</Card.Header>
      <Card.Body>
        <p>{description}</p>
        <p>Price: <span style={{ color: "orange" }}>${price}</span></p>
        <p>Stock: {stock}</p>
        <p>
          Quantity:{" "}
        </p>
        <p>
          Order Quantity:{" "}
          <input
            type="number"
            min="1"
            max={stock}
            value={quantity}
            onChange={handleOrderCountChange}
          />
        </p>
        {user ? (
          <Button
            variant="primary"
            className="btn btn-dark me-3"
            onClick={handleBuyNow}
          >
            Checkout
          </Button>
        ) : (
          <Link to="/login" className="btn btn-primary">
            Login to Buy
          </Link>
        )}
      </Card.Body>
    </Card>
  );
}
