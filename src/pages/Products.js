import {useEffect, useState, useContext} from "react";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";
import {Container, Row} from "react-bootstrap";




import ProductCard from "../components/ProductCard";

export default function Product(){


   const [products, setProducts] = useState([]);

  
  const {user} = useContext(UserContext);


  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/products`)
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setProducts(data.map(product =>{
        return(
          <ProductCard key={product._id} productProp={product} />
        )
      }))
    })
  },[])

  
  return(
    (user.isAdmin)
    ?
      <Navigate to="/" />
    :
    <>
    <Container className="loginBG">
      <h1 className= "text-center">Products</h1>
      <Row className="mt-3 mb-3">
      {products}
      </Row>
    </Container>
    </>
  )
}