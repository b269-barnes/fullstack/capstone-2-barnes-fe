import { useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import Swal from "sweetalert2";

function ShowOrder() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    // Fetch orders from the server
    fetch(`${process.env.REACT_APP_API_URL}/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrders(data);
      })
      .catch((error) => {
        console.error("Error fetching orders:", error);
      });
  }, []);

  const handleDelete = (orderId) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        // Delete order from the server
        fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/delete-order`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((res) => res.json())
          .then((data) => {
            if (data === true) {
              setOrders((prevOrders) =>
                prevOrders.filter((order) => order._id !== orderId)
              );
              Swal.fire({
                title: "Deleted order",
                icon: "success",
                text: "The order has been deleted.",
              });
            } else {
              Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please try again.",
              });
            }
          })
          .catch((error) => {
            console.error("Error deleting order:", error);
          });
      }
    });
  };

  return (
    <div className="mt-4">
      <h1>Orders</h1>
      {orders.length > 0 ? (
        <>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Order Date</th>
                <th>Product ID</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {orders.map((order) => (
                order.items.map((item, index) => (
                  <tr key={`${order._id}-${index}`}>
                    {index === 0 ? (
                      <>
                        <td rowSpan={order.items.length}>{order.purchasedOn}</td>
                        <td rowSpan={order.items.length}>{item.name}</td>
                        <td rowSpan={order.items.length}>${item.price.toFixed(2)}</td>
                        <td rowSpan={order.items.length}>{item.quantity}</td>
                        <td rowSpan={order.items.length}>${order.totalAmount.toFixed(2)}</td>
                        <td rowSpan={order.items.length}>
                          <Button
                            variant="danger"
                            size="sm"
                            onClick={() => handleDelete(order._id)}
                          >
                            Delete
                          </Button>
                        </td>
                      </>
                    ) : null}
                  </tr>
                ))
              ))}
            </tbody>
          </Table>
        </>
      ) : (
        <p>No orders yet.</p>
      )}
    </div>
  );
}

export default ShowOrder;
